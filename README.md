# Programming Competitions

A set of solved problems from a few online programming challenge sites.

Problems from are mostly solved in Kotlin and some in Java.

## Problem sites

[Advent of code 2018](https://adventofcode.com/2018)

[LeetCode](https://leetcode.com/)

[Project Euler](https://projecteuler.net/)