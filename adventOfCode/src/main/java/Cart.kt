import java.io.File

data class Cart1(var x:Int, var y: Int, var dir : Char, var isDead: Boolean = false){
    private var turnCount = 0
    fun move(trackSection:Char) {
        when (trackSection){
            '+' -> makeTurn()
            '\\' -> if (dir == '<' || dir == '>') right() else left()
            '/' -> if (dir == '<' || dir == '>') left() else right()
        }
        when (dir){
            '^' -> y--
            '>' -> x++
            'v' -> y++
            '<' -> x--
        }
    }

    private fun right(){
        dir = turnRight[dir] ?: error("")
    }

    private fun left(){
        dir = turnLeft[dir] ?: error("")
    }

    private fun makeTurn(){
        turnCount = (turnCount+1) % turns.length
        when (turns[turnCount]){
            'r' -> right()
            'l' -> left()
        }
    }

    fun collidedWith(other: Cart1) = (x == other.x) && (y == other.y)

    fun position() = x to y

    companion object {
        const val turns = "rls"
        val turnRight = mapOf('>' to 'v', 'v' to '<', '<' to '^', '^' to '>')
        val turnLeft = mapOf('>' to '^', 'v' to '>', '<' to 'v', '^' to '<')
    }
}

fun findCollisions(tracks:Array<Array<Char>>, carts:List<Cart1>) = generateSequence {
        val movingCarts = carts.map { it.copy() }.toMutableList()
        while(movingCarts.size > 1){
            movingCarts.sortWith(compareBy({it.y},{it.x}))
            movingCarts.forEach {cart ->
                if (!cart.isDead){
                    cart.move(tracks[cart.y][cart.x])
                    if (movingCarts.collisionOccurred(cart)){
                        movingCarts.forEach { if (it.collidedWith(cart)) it.isDead = true}
                        cart.position()
                    }
                }
            }
            movingCarts.removeIf { it.isDead }
        }
        // Output Position of Final Cart as a Collision
        movingCarts[0].position()
    }

fun List<Cart1>.collisionOccurred(cart: Cart1): Boolean = this.count { !it.isDead && it.collidedWith(cart)} > 1

fun solve(tracks:Array<Array<Char>>, carts: List<Cart1>): Pair<Int, Int> {
    val collisions = findCollisions(tracks,carts)
    return collisions.first() //to lastCollision
}

fun main() {
        val lines = File("adventOfCode/data/day13.data").readLines()
        val (carts, tracks: Array<Array<Char>>) = parseLines(lines)

        val (part1,part2) = solve(tracks, carts.toList())

        println("Part I: $part1")
        println("Part II: $part2")

}

private fun parseLines(lines: List<String>): Pair<MutableList<Cart1>, Array<Array<Char>>> {
    val carts = mutableListOf<Cart1>()
    val tracks: Array<Array<Char>> = lines.mapIndexed { y, line ->
        line.mapIndexed { x, c ->
            if (c in listOf('>', 'v', '<', '^')) {
                carts.add(Cart1(x, y, c))
            }
            when (c) {
                'v' -> '|'
                '^' -> '|'
                '<' -> '-'
                '>' -> '-'
                else -> c
            }
        }.toTypedArray()
    }.toTypedArray()
    return Pair(carts, tracks)
}