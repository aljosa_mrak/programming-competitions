import java.io.File
import kotlin.system.exitProcess

/**
 * Created by PP on 2.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day1.data"

    // part
    var frequency = 0
    File(fileName).forEachLine {
        frequency += it.toInt()
    }
    println("Part one solution: $frequency")

    // part 2
    val set: HashSet<Int> = HashSet()
    frequency = 0
    set.add(frequency)
    while (true) {
        File(fileName).forEachLine {
            frequency += it.toInt()
            if (set.contains(frequency)) {
                println("Part two solution: $frequency")
                exitProcess(1)
            }
            set.add(frequency)
        }
    }
}
