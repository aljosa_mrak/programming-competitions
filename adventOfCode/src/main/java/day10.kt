import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day10.data"

    var points = File(fileName).readLines().map { PointD(it) }

    var minArea = Long.MAX_VALUE
    var i = 0
    var minIdx = -1
    do {
        val currentArea = (points.maxBy { it.x }!!.x - points.minBy { it.x }!!.x) * (points.maxBy { it.y }!!.y - points.minBy { it.y }!!.y)
        if (currentArea < minArea) {
            minArea = currentArea
            minIdx = i
        }
        doStep(points)
        i++
    } while (i - minIdx < 300)
    
    points = File(fileName).readLines().map { PointD(it) }
    points = points.map { PointD(it.x + minIdx * it.velocityX, it.y + minIdx * it.velocityY) }

    val diffX = (points.maxBy { it.x }!!.x - points.minBy { it.x }!!.x)
    val diffY = (points.maxBy { it.y }!!.y - points.minBy { it.y }!!.y)
    val map = Array(diffY.toInt()+1) { Array(diffX.toInt()+1) {' '} }
    val minX = points.minBy { it.x }!!.x
    val minY = points.minBy { it.y }!!.y
    for (point in points) {
        map[(point.y - minY).toInt()][(point.x  - minX).toInt()] = '*'
    }
    for (m in map) {
        println(m.joinToString(separator = ""))
    }
// part 2
    println("Part two solution: $minIdx")
}

private fun doStep(points: List<PointD>) {
    points.forEach {
        it.x += it.velocityX
        it.y += it.velocityY
    }
}

class PointD() {
    var x: Long = 0
    var y: Long = 0
    var velocityX: Long = 0
    var velocityY: Long = 0
    
    constructor(line: String) : this() {
        val data = line.split(",").map { it.toLong() }
        x = data[0]
        y = data[1]
        velocityX = data[2]
        velocityY = data[3]
    }

    constructor(x: Long, y: Long) : this() {
        this.x = x
        this.y = y
    }
}