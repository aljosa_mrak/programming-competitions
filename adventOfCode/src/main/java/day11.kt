/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    // puzzle input
    val serialNum = 3613

    val data = Array(300) {IntArray(300)}

    // create data
    val mapped = data.mapIndexed { y, lines ->
        lines.mapIndexed { x, _ ->
            val t = (((x+1 + 10) * (y+1) + serialNum) * (x+1 + 10)).toString()
            val k = (if (t.length >= 3)  t[t.length - 3] - '0' else 0) - 5
            k
        }
    }

    // calculate cumulative sum
    val cumSum = Array(300) {IntArray(300)}
    for (i in mapped.indices) {
        for (j in mapped[i].indices) {
            cumSum[i][j] = (if (i - 1 < 0) 0 else cumSum[i - 1][j]) +
                    (if (j - 1 < 0) 0 else cumSum[i][j - 1]) -
                    (if (i - 1 < 0 || j - 1 < 0) 0 else cumSum[i - 1][j - 1]) +
                    mapped[i][j]
        }
    }

    val (idxY, idxX) = convolve(cumSum, 3)
    println("Part one solution: $idxX,$idxY")

    val resultPart2q = (1..300).maxBy { convolve(cumSum, it).third!! }
    val (idxY2, idxX2) = convolve(cumSum, resultPart2q!!)
    println("Part one solution: $idxX2,$idxY2,$resultPart2q")

}

private fun convolve(cumSum: Array<IntArray>, size: Int): Triple<Int, Int, Int?> {
    val result = Array(cumSum.size) { IntArray(cumSum[0].size) }
    for (i in (size-1) until cumSum.size) {
        for (j in (size-1) until cumSum[i].size) {
            result[i][j] = cumSum[i][j] -
                    (if (i - size < 0) 0 else cumSum[i - size][j]) -
                    (if (j - size < 0) 0 else cumSum[i][j - size]) +
                    (if (i - size < 0 || j - size < 0) 0 else cumSum[i - size][j - size])
        }
    }

    var max = Int.MIN_VALUE
    var idxI = -1
    var idxJ= -1
    for (i in result.indices) {
        for (j in result[i].indices) {
            if (result[i][j] > max) {
                max = result[i][j]
                idxI = i
                idxJ = j
            }
        }
    }

    return Triple(idxI - size+2, idxJ - size+2, max)
}