import java.io.File
import kotlin.math.abs

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day12.data"

    val reader = File(fileName).bufferedReader()
    var currentState = reader.readLine().replace("initial state: ", "") + "...."
    reader.readLine()
    val patterns = reader.readLines().map { it.split(" => ") }.map { it[0] to it[1] }.toMap()

    var totalMoveToRight = 0
    val prev = HashSet<String>()

    for (i in 0 until 200) {
        if (currentState in prev) {
            val result2 = currentState.withIndex().filter { it.value == '#' }.map { it.index.toLong() - (-57-(50000000000-118))}.sum()
            println("Part one solution: $result2")
            return
        }
        prev.add(currentState)

        val nextGen = StringBuilder(currentState.length + 10).append("..")

        // add padding to the beginning
        var num = 4 - currentState.indexOf('#')
        if (num > 0) {
            currentState = ".".repeat(num) + currentState
            totalMoveToRight += num
        } else {
            currentState = currentState.substring(abs(num))
            totalMoveToRight += num
        }

        for (j in 0 until currentState.length - 5) {
            val pattern = patterns[currentState.substring(j, j+5)]
            if (pattern != null) {
                nextGen.append(pattern)
            } else {
                nextGen.append(".")
            }
        }

        // add padding to the end
        num = 4 - (nextGen.length - 1 - nextGen.lastIndexOf('#'))
        if (num <= 4) {
            nextGen.append( ".".repeat(num))
        }

        if (i % 1000 == 0) {
            println(i)
        }

        currentState = nextGen.toString()
    }
    val result = currentState.withIndex().sumBy { if (it.value == '#') it.index-totalMoveToRight else 0 }

    println("Part one solution: $result")
}