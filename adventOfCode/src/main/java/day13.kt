import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day13.data"

    val cartsChar = charArrayOf('<', '>', 'v', '^')
    val roadChar = charArrayOf('-', '|')
    val turnChar = charArrayOf('\\', '/')

    val lines = File(fileName).readLines()
    val maxLength = lines.maxBy { it.length }!!.length

    val map = Array(lines.size) { IntArray(maxLength) }
    var carts = mutableSetOf<Cart>()

    lines.withIndex().forEach {line ->
        line.value.withIndex().forEach {
            when (it.value) {
                in cartsChar -> {
                    map[line.index][it.index] = 1 or 16
                    carts.add(Cart(it.index, line.index, it.value))
                }
                '+' -> map [line.index][it.index] = 8
                in turnChar -> map [line.index][it.index] = 2 or if (it.value == '/') 4 else 0 // set direction
                in roadChar -> map[line.index][it.index] = 1
            }
        }
    }

    while (true) {
        carts = carts.sortedWith( compareBy({ it.y }, { it.x } )).toMutableSet()

        for (cart in carts) {
            if (cart.move(map)) {
                println("Part one solution: ${cart.x},${cart.y}")
                return
            }
        }
    }

}

class Cart() {

    constructor(x: Int, y: Int, it: Char) : this() {
        this.x = x
        this.y = y
        this.direction = when (it) {
            '<' -> Pair(-1, 0)
            '>' -> Pair(1, 0)
            '^' -> Pair(0, -1)
            'v' -> Pair(0, 1)
            else -> {
                Pair(0, 0)
            }
        }
    }

    private lateinit var direction: Pair<Int, Int>
    private var nextDir = 0
    var x: Int = 0
    var y: Int = 0

    fun move(map: Array<IntArray>): Boolean {
        map[y][x] = map[y][x] and 0b1111
        when {
            map[y][x] == 1 -> {
            }
            map[y][x] and 0b10 != 0 -> {
                val dir = if ((map[y][x] and 0b100) != 0) 1 else -1
                when {
                    direction.first > 0 -> {
                        direction = Pair(0, -dir)
                    }
                    direction.first < 0 -> {
                        direction = Pair(0, dir)
                    }
                    direction.second > 0 -> {
                        direction = Pair(-dir, 0)
                    }
                    direction.second < 0 -> {
                        direction = Pair(dir, 0)
                    }
                }
            }
            map[y][x] and 0b1000 != 0 -> {
                if (nextDir != 1) {
                    when {
                        direction.first == 0 -> direction = Pair((if (nextDir == 0) 1 else -1) * direction.second, 0)
                        direction.first != 0 -> direction = Pair(0, (if (nextDir == 0) -1 else -1) * direction.first)
                    }
                }
                nextDir = (nextDir+1) % 3
            }
        }

        x += direction.first
        y += direction.second

        val collision = map[y][x] and 16 != 0
        map[y][x] = map[y][x] or 16
        return collision
    }
}
