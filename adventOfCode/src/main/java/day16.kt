import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day16.data"
    val fileNamePart2 = "adventOfCode/data/day16_part2.data"

    val instructionsExamples = File(fileName).readLines().map { InstructionExample(it) }

    val instructions = Instruction.values().withIndex().map { it.index to Instruction.values().toMutableSet() }.toMap().toMutableMap()

    val result = instructionsExamples.count { instruction ->
        Instruction.values().filter { it.check(instruction) }.size >= 3
    }
    println("Part one solution: $result")

    for (instructionExample in instructionsExamples) {
        val instructionPossibility = instructions[instructionExample.instructionCode]
        instructionPossibility?.removeAll(Instruction.values().filter { !it.check(instructionExample) })
    }

    while (instructions.isNotEmpty()) {
        val knownInstructions = instructions.filter { it.value.size == 1 }
        for (knownInstruction in knownInstructions) {
            knownInstruction.value.first().id = knownInstruction.key
            instructions.remove(knownInstruction.key)
            for (instruction in instructions) {
                instruction.value.remove(knownInstruction.value.first())
            }
        }
    }

    val executionInstructions = File(fileNamePart2).readLines().map { ExecutionInstruction(it) }
    val instructionsMap = Instruction.values().map { it.id to it }.toMap()

    val registers = intArrayOf(0, 0, 0, 0)
    for (exec in executionInstructions) {
        (instructionsMap[exec.instructionCode] ?: error("")).apply(exec, registers)
    }

    println("Part two solution: ${registers[0]}")
}

class InstructionExample() {
    lateinit var before: IntArray

    lateinit var instruction: IntArray

    lateinit var after: IntArray

    var instructionCode: Int = -1

    constructor(line: String) : this() {
        val t = line.split(";")
        before = t[0].split(",").map { it.toInt() }.toIntArray()
        instruction = t[1].split(",").map { it.toInt() }.toIntArray()
        after = t[2].split(",").map { it.toInt() }.toIntArray()
        instructionCode = instruction[0]
    }
}

class ExecutionInstruction() {
    lateinit var instruction: IntArray
    var instructionCode: Int = -1

    constructor(line: String) : this() {
        instruction = line.split(" ").map { it.toInt() }.toIntArray()
        instructionCode = instruction[0]
    }
}

enum class Instruction {

    addr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] + tmpBefore[i.instruction[2]]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] + input[i.instruction[2]]
        }
    },

    addi {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] + i.instruction[2]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] + i.instruction[2]
        }
    },

    mulr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] * tmpBefore[i.instruction[2]]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] * input[i.instruction[2]]
        }
    },

    muli {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] * i.instruction[2]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] * i.instruction[2]
        }
    },

    banr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] and tmpBefore[i.instruction[2]]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] and input[i.instruction[2]]
        }
    },

    bani {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] and i.instruction[2]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] and i.instruction[2]
        }
    },

    borr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] or tmpBefore[i.instruction[2]]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] or input[i.instruction[2]]
        }
    },

    bori {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]] or i.instruction[2]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]] or i.instruction[2]
        }
    },

    setr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = tmpBefore[i.instruction[1]]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = input[i.instruction[1]]
        }
    },

    seti {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = i.instruction[1]
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = i.instruction[1]
        }
    },

    gtir {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (i.instruction[1] > tmpBefore[i.instruction[2]]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (i.instruction[1] > input[i.instruction[2]]) 1 else 0
        }
    },

    gtri {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (tmpBefore[i.instruction[1]] > i.instruction[2]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (input[i.instruction[1]] > i.instruction[2]) 1 else 0
        }
    },

    gtrr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (tmpBefore[i.instruction[1]] > tmpBefore[i.instruction[2]]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (input[i.instruction[1]] > input[i.instruction[2]]) 1 else 0
        }
    },

    eqir {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (i.instruction[1] == tmpBefore[i.instruction[2]]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (i.instruction[1] == input[i.instruction[2]]) 1 else 0
        }
    },

    eqri {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (tmpBefore[i.instruction[1]] == i.instruction[2]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (input[i.instruction[1]] == i.instruction[2]) 1 else 0
        }
    },

    eqrr {
        override fun check(i: InstructionExample): Boolean {
            val tmpBefore = i.before.copyOf()
            tmpBefore[i.instruction[3]] = if (tmpBefore[i.instruction[1]] == tmpBefore[i.instruction[2]]) 1 else 0
            return tmpBefore.contentEquals(i.after)
        }

        override fun apply(i: ExecutionInstruction, input: IntArray) {
            input[i.instruction[3]] = if (input[i.instruction[1]] == input[i.instruction[2]]) 1 else 0
        }
    };

    var id: Int = -1

    abstract fun check(i: InstructionExample): Boolean

    abstract fun apply(i: ExecutionInstruction, input: IntArray)
}
