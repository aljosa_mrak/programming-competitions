import java.io.File
import java.util.*

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day17.data"

    val parsedLines = File(fileName).readLines().map { ParsedLine(it) }
    val minX = parsedLines.minBy { it.x.min() }!!.x.min()
    val maxX = parsedLines.minBy { it.x.max() }!!.x.min()
    val maxY = parsedLines.minBy { it.y.max() }!!.y.min()

    val map = Array(maxX - minX) { IntArray(maxY) }
    parsedLines.forEach { parsedLine ->
        if (parsedLine.x.range != null) {
            for (i in parsedLine.x.range!![0] .. parsedLine.x.range!![1]) {
                map[i][parsedLine.y.value]
            }
        } else {
            for (i in parsedLine.y.range!![0] .. parsedLine.y.range!![1]) {
                map[parsedLine.x.value][i]
            }
        }
    }

    val queue = ArrayDeque<Pair<Int, Int>>()
    queue.add(Pair(500, 0))

    while (queue.isNotEmpty()) {
        val current = queue.first
        map[current.first][current.second] = 2
        if (map[current.first][current.second+1] == 0) {
            queue.add(Pair(current.first, current.second+1))
        } else if (map[current.first][current.second+1] == 1) {
            queue.add(Pair(current.first+1, current.second))
            queue.add(Pair(current.first-1, current.second))
        }
    }
}

class ParsedLine() {

    lateinit var x: Range
    lateinit var y: Range

    constructor(line: String) : this() {
        val split = line.split(", ")
        if (split[0].contains("x")) {
            this.x = Range(split[0])
            this.y = Range(split[1])
        } else {
            this.y = Range(split[0])
            this.x = Range(split[1])
        }
    }
}

class Range() {
    var value: Int = 0

    var range: IntArray? = null
    constructor(s: String) : this() {
        if (s.contains("..")) {
            this.range = s.substring(2).split("..").map { it.toInt() }.toIntArray().sortedArray()
        } else {
            this.value = s.substring(2).toInt()
        }
    }

    fun min(): Int {
        return if (range != null) range!![0] else value
    }

    fun max(): Int {
        return if (range != null) range!![1] else value
    }
}
