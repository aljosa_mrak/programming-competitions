import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day19.data"

    val reader = File(fileName).bufferedReader()
    val ip = reader.readLine().replace("#ip ", "").toInt()
    val instructions = reader.readLines().map { Instruction19(it) }


    val registers = LongArray(6)
    runInstructions(registers, ip, instructions)
    println("Part one solution: ${registers[0]}")

    var sum = 0L
    for (i in 1 .. 10551378L) {
        if (10551378 % i  == 0L) {
            sum += i
        }
    }
    println("Part two solution: $sum")
}

private fun runInstructions(registers: LongArray, ip: Int, instructions: List<Instruction19>) {
    var pre5 = -1L
    var pre0 = -1L
    while (true) {
        if (registers[ip] < 0 || registers[ip] >= instructions.size) {
            break
        }

        val instruction = instructions[registers[ip].toInt()]
        instruction.command.apply(instruction, registers)
        if (registers[5] != pre5) {
            println(registers.contentToString())
            pre5 = registers[5]
            if (registers[5] == 15L) {
                registers[5] = 10551378L - 5
            }
        }
        if (registers[0] != pre0) {
            println(registers.contentToString())
            pre0 = registers[0]
        }
        registers[ip]++
    }
}

class Instruction19() {
    lateinit var command: Command

    lateinit var instruction: List<Long>

    constructor(line: String) : this() {
        val t = line.split(" ")
        command = Command.valueOf(t[0])
        instruction = t.subList(1, t.size).map { it.toLong() }
    }
}

enum class Command {

    addr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] + input[i.instruction[1].toInt()]
        }
    },

    addi {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] + i.instruction[1]
        }
    },

    mulr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] * input[i.instruction[1].toInt()]
        }
    },

    muli {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] * i.instruction[1]
        }
    },

    banr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] and input[i.instruction[1].toInt()]
        }
    },

    bani {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] and i.instruction[1]
        }
    },

    borr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] or input[i.instruction[1].toInt()]
        }
    },

    bori {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()] or i.instruction[1]
        }
    },

    setr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = input[i.instruction[0].toInt()]
        }
    },

    seti {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = i.instruction[0]
        }
    },

    gtir {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (i.instruction[0] > input[i.instruction[1].toInt()]) 1 else 0
        }
    },

    gtri {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (input[i.instruction[0].toInt()] > i.instruction[1]) 1 else 0
        }
    },

    gtrr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (input[i.instruction[0].toInt()] > input[i.instruction[1].toInt()]) 1 else 0
        }
    },

    eqir {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (i.instruction[0] == input[i.instruction[1].toInt()]) 1 else 0
        }
    },

    eqri {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (input[i.instruction[0].toInt()] == i.instruction[1]) 1 else 0
        }
    },

    eqrr {
        override fun apply(i: Instruction19, input: LongArray) {
            input[i.instruction[2].toInt()] = if (input[i.instruction[0].toInt()] == input[i.instruction[1].toInt()]) 1 else 0
        }
    };

    abstract fun apply(i: Instruction19, input: LongArray)
}
