
import java.io.File
import kotlin.math.abs

/**
 * Created by PP on 2.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day2.data"

    // part 2
    var seconds = 0
    var thirds = 0
    File(fileName).forEachLine { line ->
        val countedUniques = line.toCharArray().toList().groupingBy { it }.eachCount()

        if (countedUniques.containsValue(2)) {
            seconds++
        }
        if (countedUniques.containsValue(3)) {
            thirds++
        }
    }
    println("Part one solution: ${seconds * thirds}")

    // part 2
    val sortedLines = File(fileName).readLines()
    for(i in sortedLines) {
        for(j in sortedLines) {
            if (compare(i, j) == 1) {
                println("Part two solution: ${j.filterIndexed {idx, it -> it == i[idx]}}")      // get only matching
                return
            }
        }
    }
}

fun compare(s1: String?, s2: String?): Int {
    var diff = 0
    if (s1 != null && s2 != null) {
        for (i in 0 until minOf(s1.length, s2.length)) {
            if (s1[i] != s2[i]) {
                diff++
            }
        }
    }
    if (s1 != null && s2 != null) {
        return diff + abs(s1.length - s2.length)
    }
    return 100
}