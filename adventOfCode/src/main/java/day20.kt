import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day20.data"

    File(fileName).readText()

    listOf(/*"^WNE\$",
            "^ENWWW(NEEE|SSE(EE|N))\$",*/
            "^E(NEWS|)S(WNSE|W(W|WW))E(SW|W(W|))N\$").forEach {//8
        val result = countDoors(1, it.length-2, it)
        println(result)
    }
}

fun countDoors(startIdx: Int, end: Int, data: String): Int {
    var doors = 0
    var idx = startIdx
    while (true) {
        if (idx > end) {
            return idx - startIdx
        }
        when (data[idx]) {
            '(' -> {
                var openedBrackets = 1
                var newIdx = idx+1
                while (openedBrackets != 0) {
                    if (data[newIdx] == '(') {
                        openedBrackets++
                    }
                    if (data[newIdx] == ')') {
                        openedBrackets--
                    }
                    newIdx++
                }
                return idx-startIdx + countDoors(idx+1, newIdx-2, data) + countDoors(newIdx, end, data)
            }
            '|' -> {
                val secondGroup = countDoors(idx+1, end, data)
                return if (secondGroup == 0) 0 else maxOf(secondGroup, idx-startIdx)
            }
            else -> {
                doors++
                idx++
            }
        }
    }
}
