import java.util.*

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val depth = 6969
    val target = intArrayOf(9,796)

    var type = createType(target, depth, intArrayOf(1, 1))

    val result = type.flatten().sum()
    println("Part one solution: $result")

    type = createType(target, depth, intArrayOf(30, 300))

    val queue = TreeSet<Vertex>()
    val startVertex = Vertex(0, 0, type[0][0], 1)
    startVertex.dist = 0
    queue.add(startVertex)

    while (!queue.isEmpty()) {
        // vertex with shortest distance (first iteration will return source)
        val current = queue.pollFirst()

        // if target
        if (current.x == target[0] && current.y == target[1]) {
            println("Part one solution: ${current.dist}")
            current.print()
            break
        }

        //look at distances to each neighbour
        for (neighbour in generateNext(current, type, queue, target)) {
            val newDist = current.dist + if (current.equipment != neighbour.equipment) 8 else 1

            if (newDist < neighbour.dist) { // shorter path to neighbour found
                queue.remove(neighbour)
                queue.removeIf { it.x == neighbour.x && it.y == neighbour.y && it.equipment == neighbour.equipment && it.dist < newDist }
                neighbour.dist = newDist
                neighbour.hevristics = 0//abs(target[0] - neighbour.x) + abs(target[1] - neighbour.y)
                neighbour.previous = current
                queue.add(neighbour)
            }
        }
    }
}

private fun createType(target: IntArray, depth: Int, increasedSize: IntArray): List<List<Long>> {
    val erosionLevel = Array(target[0] + increasedSize[0]) { LongArray(target[1] + increasedSize[1]) { 0 } }

    for (x in 0 until erosionLevel.size) {
        erosionLevel[x][0] = (x * 16807L + depth) % 20183
    }
    for (y in 0 until erosionLevel[0].size) {
        erosionLevel[0][y] = (y * 48271L + depth) % 20183
    }
    for (x in 1 until erosionLevel.size) {
        for (y in 1 until erosionLevel[x].size) {
            erosionLevel[x][y] = (erosionLevel[x - 1][y] * erosionLevel[x][y - 1] + depth) % 20183
        }
    }
    erosionLevel[target[0]][target[1]] = (0L + depth) % 20183

    return erosionLevel.map { it -> it.map { (it + depth) % 3 } }
}

fun generateNext(vertex: Vertex, data: List<List<Long>>, queue: TreeSet<Vertex>, target: IntArray): List<Vertex> {
    val nexts = mutableListOf<Vertex>()
    for (neighbour in listOf(
            if (vertex.x > 0) Vertex(vertex.x-1, vertex.y, data[vertex.x-1][vertex.y], -1) else null,
            if (vertex.y > 0) Vertex(vertex.x, vertex.y-1, data[vertex.x][vertex.y-1], -1) else null,
            if (vertex.y < data[vertex.x].size-1) Vertex(vertex.x, vertex.y+1, data[vertex.x][vertex.y+1], -1) else null,
            if (vertex.x < data.size-1) Vertex(vertex.x+1, vertex.y, data[vertex.x+1][vertex.y], -1) else null)) {
        if (neighbour == null) {
            continue
        } else if (vertex.previous?.x == neighbour.x && vertex.previous?.y == neighbour.y) {
            continue
        } else if (queue.contains(neighbour)) {
            continue
        } else if (neighbour.x == target[0] && neighbour.y == target[1]) {
            nexts.add(Vertex(neighbour.x, neighbour.y, neighbour.type, 1))
        } else if (validEquipment(neighbour.type, vertex.equipment)) {
            nexts.add(Vertex(neighbour.x, neighbour.y, neighbour.type, vertex.equipment))
        } else {
            for (equipment in listOf(0, 1, 2).minus(vertex.equipment)) {
                if (validEquipment(vertex.type, equipment)) {
                    nexts.add(Vertex(neighbour.x, neighbour.y, neighbour.type, equipment))
                }
            }
        }
    }
    return nexts
}
fun validEquipment(type: Long, equipment: Int): Boolean {
    return when {
        type == 0L && equipment == 1 -> true
        type == 0L && equipment == 2 -> true
        type == 1L && equipment == 0 -> true
        type == 1L && equipment == 2 -> true
        type == 2L && equipment == 0 -> true
        type == 2L && equipment == 1 -> true
        else -> {
            false
        }
    }
}

/** One vertex of the graph, complete with mappings to neighbouring vertices */
class Vertex(val x: Int, val y: Int, val type: Long, val equipment: Int) : Comparable<Vertex> {

    var dist = Int.MAX_VALUE
    var hevristics = Int.MAX_VALUE
    var previous: Vertex? = null

    override fun compareTo(other: Vertex): Int {
        if (dist + hevristics == other.dist + other.hevristics) {
            if (x == other.x) {
                if (y == other.y) {
                    return equipment.compareTo(other.equipment)
                }
                return y.compareTo(other.y)
            }
            return x.compareTo(other.x)
        }
        return (dist + hevristics).compareTo(other.dist + other.hevristics)
    }

    override fun toString() = "($x,$y - ${dist + hevristics}; $equipment)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vertex

        if (x != other.x) return false
        if (y != other.y) return false
        if (equipment != other.equipment) return false

        return true
    }

    fun print() {
        if (previous != null) {
            previous!!.print()
            println("$x:$y - $dist, ${type()}, ${equipment()}${if (!validEquipment(this.previous!!.type, this.equipment)) ", change" else ""}")
        } else {
            println("$x:$y - $dist, ${type()}, ${equipment()}")
        }
    }

    private fun type(): String {
        return when (type) {
            0L -> "rocky"
            1L -> "wet"
            2L -> "narrow"
            else -> ""
        }
    }

    private fun equipment(): String {
        return when (equipment) {
            0 -> "neither"
            1 -> "torch"
            2 -> "climbing"
            else -> ""
        }
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        result = 31 * result + equipment
        return result
    }
}