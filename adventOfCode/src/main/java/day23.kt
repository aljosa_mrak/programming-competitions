import java.io.File
import kotlin.math.abs

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day23.data"

    val nanobots = File(fileName).readLines().map { it ->
        it.replace("pos=<", "").replace(">, r=", ",").split(",").map { it.toInt() } }

    val nanobotMax = nanobots.maxBy { it[3] }
    val count = nanobots.count { abs(it[0] - nanobotMax!![0]) + abs(it[1] - nanobotMax[1]) + abs(it[2] - nanobotMax[2]) <= nanobotMax[3] }

    println("Part one solution: $count")
}