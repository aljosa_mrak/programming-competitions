import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day24.data"

    val (armies, groups) = parseInput(fileName)
    armies[0].attackingArmy = armies[1]
    armies[1].attackingArmy = armies[0]

    work(groups, armies)

    for (army in armies) {
        println(army.groups.sumBy { it.numUnits })
    }


    var boost = 89
    val boostChange = 0
    while (true) {
        val (armies, groups) = parseInput(fileName)
        armies[0].attackingArmy = armies[1]
        armies[1].attackingArmy = armies[0]

        boost++
        groups.forEach {
            if (it.army.armyName.contains("Immune")) {
                it.attack += boost
            }
        }

        work(groups, armies)
        if (armies.first { it.armyName.contains("Infection") }.groups.all { it.numUnits <= 0 } ) {
            for (army in armies) {
                print(army.armyName + " ")
                println(army.groups.filter { it.numUnits > 0 }.sumBy { it.numUnits })
            }
            break
        }
    }
    println("DS")
    println(boostChange)
    println(boost)
}

fun work(groups: MutableList<Group>, armies: List<Army>): List<Army> {
    var end = false
    while (!end) {
        // select
        val attackingGroupsCopy = armies.map { it -> it to it.attackingArmy.groups.filter { it.numUnits > 0 }.toMutableList() }.toMap()
        var sorted = groups.sortedWith(compareBy( { -it.numUnits * it.attack }, { -it.initiative } ))

        sorted.forEach {
            it.attackGroup = mostAttack(it, attackingGroupsCopy[it.army] ?: error(""))
            if (it.attackGroup != null) {
                (attackingGroupsCopy[it.army] ?: error("")).remove(it.attackGroup!!)
            }
        }

        // attack
        var killed = false
        sorted = groups.sortedWith(compareBy { -it.initiative })
        sorted.forEach {
            if (it.attackGroup != null && it.numUnits > 0 && !it.attackGroup!!.immune.contains(it.attackType)) {
                val attack  = (if (it.attackGroup!!.weakness.contains(it.attackType)) 2 else 1) * it.numUnits * it.attack
                it.attackGroup!!.numUnits -= (attack / it.attackGroup!!.hitPoints)
                if ((attack / it.attackGroup!!.hitPoints) > 0) {
                    killed = true
                }
            }
            it.attackGroup = null
        }

        if (!killed) {
            return emptyList()
        }

        groups.removeAll { it.numUnits <= 0 }

        for (army in armies) {
            if (army.groups.all { it.numUnits <= 0 }) {
                end = true
                break
            }
        }
    }
    return armies
}

fun mostAttack(group: Group, groups: MutableList<Group>): Group? {
    val maxGroup = groups.maxWith(compareBy( { !it.immune.contains(group.attackType) },
            { it.weakness.contains(group.attackType) }, { it.numUnits * it.attack }, { it.initiative } ))
    if (maxGroup == null || maxGroup.immune.contains(group.attackType)) {
        return null
    }
    return maxGroup
}

fun parseInput(fileName: String): Pair<MutableList<Army>, MutableList<Group>> {
    val groupRegex = "(\\d+) units each with (\\d+) hit points (?:\\((.*)?\\))? ?with an attack that does (\\d+) (\\w+) damage at initiative (\\d+)".toRegex()

    val armies = mutableListOf<Army>()
    val groups = mutableListOf<Group>()
    val reader = File(fileName).bufferedReader()
    while (true) {
        var line: String? = reader.readLine() ?: break
        val army = Army(line!!.replace(":", ""))
        while (true) {
            line = reader.readLine()
            if (line == null || line == "") {
                break
            }
            groupRegex.matchEntire(line)
                    ?.destructured
                    ?.let { (numUnits, hitPoints, weaknessStrengthString, attack, attackType, initiative) ->
                        val group = Group(army, numUnits.toInt(), hitPoints.toInt(), weaknessStrengthString, attack.toInt(),
                                attackType, initiative.toInt())
                        groups.add(group)
                        army.groups.add(group)
                    }
                    ?: throw IllegalArgumentException("Bad input '$line'")
        }
        armies.add(army)
    }
    return Pair(armies, groups)
}

class Group(val army: Army, var numUnits: Int, val hitPoints: Int, var attack: Int, val attackType: String, val initiative: Int) {

    var immune: List<String> = mutableListOf()
    var weakness: List<String> = mutableListOf()
    var attackGroup: Group? = null

    constructor(army: Army, numUnits: Int, hitPoints: Int, weaknessStrengthString: String, attack: Int, attackType: String, initiative: Int) :
            this(army, numUnits, hitPoints, attack, attackType, initiative) {
        weaknessStrengthString.split(";").forEach { type ->
            if (type != "") {
                val data = type.substring(type.indexOf("to ") + 3).split(",").map { it.trim() }
                if (type.contains("immune")) {
                    immune = data
                } else {
                    weakness = data
                }
            }
        }
    }

    override fun toString(): String {
        return "Group(numUnits=$numUnits, hitPoints=$hitPoints, attackType='$attackType', initiative=$initiative, immune=$immune, weakness=$weakness, attackGroup=${attackGroup?.hashCode()})"
    }
}

class Army(val armyName: String) {
    val groups = mutableListOf<Group>()
    lateinit var attackingArmy: Army
}
