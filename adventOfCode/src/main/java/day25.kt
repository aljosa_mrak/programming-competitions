import java.io.File
import kotlin.math.abs

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day25.data"

    val constellations = mutableListOf<MutableList<IntArray>>()

    File(fileName).forEachLine { line ->
        val point = line.split(",").map { it.toInt() }.toIntArray()
        var added = false
        var changedConstellation: MutableList<IntArray>? = null
        constellations.forEach { constellation ->
            if (constellation.any { manhattanDist(point, it) <= 3 } ) {
                constellation.add(point)
                changedConstellation = constellation
                added = true
                return@forEach
            }
        }
        if (!added) {
            constellations.add(mutableListOf(point))
        }

        val toBeRemoved = mutableListOf<MutableList<IntArray>>()
        if (changedConstellation != null) {
            for (constellation2 in constellations) {
                if (changedConstellation == constellation2) {
                    continue
                }
                if (changedConstellation!!.any { point1 -> constellation2.any { point2 -> manhattanDist(point1, point2) <= 3 } }) {
                    changedConstellation!!.addAll(constellation2)
                    toBeRemoved.add(constellation2)
                }
            }
            constellations.removeAll(toBeRemoved)
        }
    }

    println(constellations.size)
}

fun manhattanDist(point1: IntArray, point2: IntArray): Int {
    return (point1.indices).sumBy { abs(point1[it] - point2[it])  }
}