
import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day3.data"

    var fabric = Array(1000) { Array(1000) {0} }

    // part 1
    File(fileName).forEachLine {line ->
        val data = line.substring(line.indexOf('@')+1).trim().split(": ")
        val (x, y) = data[0].split(",").map { Integer.parseInt(it) }
        val (w, h) = data[1].split("x").map { Integer.parseInt(it) }
        for (i in x until x+w) {
            for (j in y until y+h) {
                fabric[i][j]++
            }
        }
    }
    println("Part one solution: ${fabric.flatten().filter { it > 1 }.size}")

    // part 2
    val all = HashSet<Int>()
    fabric = Array(1000) { Array(1000) {0} }

    File(fileName).forEachLine {line ->
        val id = Integer.parseInt(line.substring(line.indexOf('#')+1, line.indexOf('@')).trim())
        all.add(id)
        val data = line.substring(line.indexOf('@')+1).trim().split(": ")
        val (x, y) = data[0].split(",").map { Integer.parseInt(it) }
        val (w, h) = data[1].split("x").map { Integer.parseInt(it) }
        for (i in x until x+w) {
            for (j in y until y+h) {
                if (fabric[i][j] == 0) {
                    fabric[i][j] = id
                } else {
                    all.remove(id)
                    all.remove(fabric[i][j])
                }
            }
        }
    }
    println("Part two solution: ${all.first()}")
}