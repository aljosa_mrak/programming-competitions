
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Created by PP on 6.12.2018.
 */

val dateTimeStrToLocalDateTime: (String) -> LocalDateTime = {
    LocalDateTime.parse(it.substring(1, it.indexOf("]")), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
}

fun main() {
    val fileName = "adventOfCode/data/day4.data"

    val data = HashMap<Int, IntArray>()

    // part 1
    var id = -1
    var start = -1
    var end: Int
    File(fileName).readLines().sortedBy(dateTimeStrToLocalDateTime).forEach { line ->
        if (line.contains("Guard #")) {
            id = line.substring(line.indexOf("#") + 1, line.indexOf(" begins shift")).toInt()
        }
        if (line.contains("falls asleep")) {
            start = line.substring(line.indexOf(":") + 1, line.indexOf("]")).toInt()
        }
        if (line.contains("wakes up") && id != -1) {
            end = line.substring(line.indexOf(":") + 1, line.indexOf("]")).toInt()
            addSleepPeriod(data, id, start, end)
            end = -1
            start = -1
        }
    }
    id = data.maxBy { it.value.sum() }?.key ?: -1

    var maxData = data[id]
    var maxMin = maxData?.indices?.maxBy { maxData!![it] }
    println("Part one solution: ${id * maxMin!!}")

    // part 2
    id = data.maxBy { it.value.max()!! }?.key ?: -1

    maxData = data[id]
    maxMin = maxData?.indices?.maxBy { maxData[it] }
    println("Part two solution: ${id * maxMin!!}")
}

fun addSleepPeriod(map: MutableMap<Int, IntArray>, id: Int, start: Int, end: Int) {
    val array: IntArray
    if (map.contains(id)) {
        array = map[id]!!
    } else {
        array = IntArray(100)
        map[id] = array
    }

    for (i in start until end) {
        array[i]++
    }
}
