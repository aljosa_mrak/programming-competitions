
import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day5.data"

    val data = File(fileName).readText()

    // part 1
    val removed = algorithm(data)
    println("Part two solution: ${removed.filter { it == 0 }.size}")

    // part 2
    println("Part two solution: ${data.toLowerCase().toCharArray().distinct().map {
        algorithm(data.replace(it.toString(), "", true))
    }.minBy { it -> it.filter { it == 0 }.size }!!.filter { it == 0 }.size}")
}

fun algorithm(data: String): IntArray {
    var i = 0
    var j = 1
    val removed = IntArray(data.length)
    while (i != j && j < data.length) {
        if (data[i] != data[j] && data[i].equals(data[j], true)) {
            removed[i] = 1
            removed[j] = 1
            j++
            while (i > 0 && removed[--i] != 0);
            if (i < 0) {
                i = j
                j = i+1
            }
        } else {
            i = j
            j = i+1
        }
    }

    return removed
}