import java.io.File
import java.util.*
import kotlin.math.abs

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day6.data"

    val points = File(fileName).readLines().map { it -> it.split(", ").map { it.toInt() } }

    val maxX = points.maxBy { it[0] }!![0]
    val maxY = points.maxBy { it[1] }!![1]

    val data = Array(maxX) {arrayOfNulls<PointData>(maxY)}

    // part 1
    val bfsQueue = ArrayDeque<ProcessEvent>()
    points.forEachIndexed { id: Int, point:List<Int> -> bfsQueue.add(ProcessEvent(id, point, -1)) }

    loop@ while (bfsQueue.isNotEmpty()) {
        val processEvent = bfsQueue.poll()

        if (processEvent.point[0] < 0 || processEvent.point[1] < 0 || processEvent.point[0] >= data.size || processEvent.point[1] >= data[processEvent.point[0]].size) {
            continue
        }
        val pointData = data[processEvent.point[0]][processEvent.point[1]]

        if (pointData == null) {
            data[processEvent.point[0]][processEvent.point[1]] = PointData(processEvent.id, processEvent.distance + 1)
        } else if (pointData.distance > processEvent.distance+1) {
            data[processEvent.point[0]][processEvent.point[1]] = PointData(processEvent.id, processEvent.distance + 1)
        } else if (pointData.distance == processEvent.distance+1 && pointData.id != processEvent.id) {
            data[processEvent.point[0]][processEvent.point[1]] = PointData(-1, processEvent.distance + 1)
            continue@loop
        } else {
            continue@loop
        }

        bfsQueue.add(ProcessEvent(processEvent.id, listOf(processEvent.point[0]-1, processEvent.point[1]), processEvent.distance+1))
        bfsQueue.add(ProcessEvent(processEvent.id, listOf(processEvent.point[0]+1, processEvent.point[1]), processEvent.distance+1))
        bfsQueue.add(ProcessEvent(processEvent.id, listOf(processEvent.point[0], processEvent.point[1]-1), processEvent.distance+1))
        bfsQueue.add(ProcessEvent(processEvent.id, listOf(processEvent.point[0], processEvent.point[1]+1), processEvent.distance+1))
    }

    val invalidIds = data[0].toMutableSet()
    invalidIds.addAll(data[data.size-1])
    invalidIds.addAll(data.map { it[0] })
    invalidIds.addAll(data.map { it[it.size-1] })

    println("Part one solution: " +
            data.flatten().groupingBy { it!!.id }.eachCount().filter { it -> !invalidIds.map { it!!.id }.contains(it.key) }.maxBy { it.value }!!.value)

    // part 2
    val dataPart2 = Array(maxX) {IntArray(maxY)}
    println("Part two solution: ${dataPart2.mapIndexed { x, it -> it.mapIndexed { y, _ -> points.sumBy { p -> abs(x-p[0]) + abs(y-p[1]) } } }
            .flatten().filter { it < 10000 }.size}")
}

class PointData(val id: Int, val distance: Int)
class ProcessEvent(val id: Int, val point: List<Int>, val distance: Int)