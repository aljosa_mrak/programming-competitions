
import java.io.File
import java.util.*

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day7.data"

    // read points
    val rawData = File(fileName).readLines().map {listOf(it.substring(5, 6), it.substring(36, 37))}

    val points = (rawData.map { it[0] } + rawData.map { it[1] }).distinct().map { it to Point(it) }.toMap()

    // build graph
    rawData.forEach {
        val point1 = points[it[0]]
        val point2 = points[it[1]]
        point2!!.preconditions.add(point1!!)
        point1.releases.add(point2)
    }

    val freePoints = TreeSet<Point>()
    freePoints.addAll(points.filter { it.value.preconditions.isEmpty() }.map { it.value })
    val toProcessPoints = points.map { it.value }.toMutableSet()
    val result = StringBuilder()
    while (toProcessPoints.isNotEmpty()) {
        val currPoint = freePoints.first()
        currPoint.releases.forEach {
            it.preconditions.remove(currPoint)
            if (it.preconditions.isEmpty()) {
                freePoints.add(it)
            }
        }
        toProcessPoints.remove(currPoint)
        freePoints.remove(currPoint)
        result.append(currPoint.id)
    }

    println("Part one solution: $result.toString()")
}

class Point(val id: String): Comparable<Point> {
    override fun compareTo(other: Point): Int {
        return id.compareTo(other.id)
    }

    val preconditions = mutableSetOf<Point>()
    val releases = mutableSetOf<Point>()
}
