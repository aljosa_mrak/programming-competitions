
import java.io.File
import java.util.*

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day7.data"

    // read points
    val rawData = File(fileName).readLines().map {listOf(it.substring(5, 6), it.substring(36, 37))}

    val points = (rawData.map { it[0] } + rawData.map { it[1] }).distinct().map { it to Point(it) }.toMap()

    // build graph
    rawData.forEach {
        val point1 = points[it[0]]
        val point2 = points[it[1]]
        point2!!.preconditions.add(point1!!)
        point1.releases.add(point2)
    }

    val freePoints = PriorityQueue<Point>()
    freePoints.addAll(points.filter { it.value.preconditions.isEmpty() }.map { it.value })
    val toProcessPoints = points.map { it.value }.toMutableSet()
    val jobTimes = mutableListOf<Job>()
    var time = 0
    var timeStep: Int
    while (toProcessPoints.isNotEmpty()) {
        for (i in 0 until minOf(freePoints.size, 5 - jobTimes.size)) {
            val point = freePoints.poll()
            jobTimes.add(Job(point.id[0] - 'A' + 60 + 1, point.id))
        }

        timeStep = jobTimes.minBy { it.time }!!.time
        time += timeStep
        jobTimes.filter { it.time == timeStep }.forEach {job ->
            val currPoint = points[job.id] ?: error("")
            currPoint.releases.forEach {point ->
                point.preconditions.remove(currPoint)
                if (point.preconditions.isEmpty()) {
                    freePoints.add(point)
                }
            }
            toProcessPoints.remove(currPoint)
        }
        jobTimes.removeIf { it.time == timeStep }
        jobTimes.forEach {  it.time -= timeStep }
    }

    println("Part two solution: $time")
}

class Job(var time: Int, val id: String)