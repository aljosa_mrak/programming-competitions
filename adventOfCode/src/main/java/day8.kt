
import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day8.data"

    // read points
    val rawData = File(fileName).readText().split(" ").map { it.toInt() }

    // part 2
    println("Part one solution: ${recPart1(0, rawData).second}")

    // part 2
    println("Part two solution: ${recPart2(0, rawData).second}")
}

fun recPart1(orgIdx: Int, rawData: List<Int>): Pair<Int, Int> {
    val numMetadata = rawData[orgIdx+1]
    var idx = orgIdx + 2
    var result = 0
    for (i in 1..rawData[orgIdx]) {
        val p = recPart1(idx, rawData)
        idx = p.first
        result += p.second
    }
    return Pair(idx + numMetadata, rawData.subList(idx, idx+numMetadata).sum() + result)
}

fun recPart2(orgIdx: Int, rawData: List<Int>): Pair<Int, Int> {
    val numMetadata = rawData[orgIdx+1]
    var idx = orgIdx + 2
    val childNodeValue = HashMap<Int, Int>()

    if (rawData[orgIdx] == 0) {
        return Pair(idx + numMetadata, rawData.subList(idx, idx+numMetadata).sum())
    }

    for (i in 1..rawData[orgIdx]) {
        val p = recPart2(idx, rawData)
        idx = p.first
        childNodeValue[i] = p.second
    }
    return Pair(idx + numMetadata, rawData.subList(idx, idx+numMetadata).map { childNodeValue[it] }.sumBy { it ?: 0 })
}