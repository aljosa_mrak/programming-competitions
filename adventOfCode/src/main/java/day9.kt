import java.io.File

/**
 * Created by PP on 6.12.2018.
 */

fun main() {
    val fileName = "adventOfCode/data/day9.data"

    val (numPlayers, numMarbles) = File(fileName).readText().split(",").map { it.toInt() }

    // part 1
    println("Part one solution: ${algorithm(numPlayers, numMarbles).max()}")

    // part 2
    println("Part two solution: ${algorithm(numPlayers, numMarbles * 100).max()}")
}

private fun algorithm(numPlayers: Int, numMarbles: Int): LongArray {
    val players = LongArray(numPlayers)
    var currentMarble = Marble(0)

    var currentPlayer = 0
    for (i in 1..numMarbles) {
        if (i % 23 == 0) {
            players[currentPlayer] += i.toLong()
            for (j in 0 until 6) {
                currentMarble = currentMarble.previous
            }
            players[currentPlayer] += currentMarble.removePreviousMarble()
        } else {
            currentMarble = currentMarble.next.insertNewNextMarble(i.toLong())
        }
        currentPlayer = (currentPlayer + 1) % numPlayers
    }
    return players
}

class Marble(private var value: Long) {
    var previous: Marble = this
    var next: Marble = this

    fun insertNewNextMarble(i: Long): Marble {
        val newMarble = Marble(i)
        val currentNext = next
        next = newMarble
        newMarble.previous = this
        newMarble.next = currentNext
        currentNext.previous = newMarble
        return newMarble
    }

    fun removePreviousMarble(): Long {
        val pre = previous
        val newPrevious = previous.previous
        newPrevious.next = this
        this.previous = newPrevious
        return pre.value
    }
}
