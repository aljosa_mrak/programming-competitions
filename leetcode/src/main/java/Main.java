import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println(findMinStep("BGGRRYY", "BBYRG") == 5);
        System.out.println(findMinStep("WWRRBBWW", "WRBRW") == 2);
        System.out.println(findMinStep("W", "WW") == 2);
        System.out.println(findMinStep("WWRRBBWW", "WRBRW"));
    }

    public static int findMinStep(String board, String hand) {
        Comparator<Node> comparator = (node, t1) -> node.boardDifferent() + node.depth - (t1.boardDifferent() + node.depth);
        Queue<Node> stack = new PriorityQueue<>(10, comparator);
        stack.add(new Node(board, hand, 0));

        while (stack.size() > 0) {
            Node curr = stack.poll();
            curr.removeTriples();
            if (curr.board.size() == 0) {
                return curr.depth;
            } else {
                stack.addAll(generateNext(curr));
            }
        }
        return -1;
    }

    public static LinkedList<Node> generateNext(Node node) {
        LinkedList<Node> generated = new LinkedList<>();
        for (int c = 0; c < 5; ++c) {
            if (node.hand[c] > 0) {
                for (int i = 0; i < node.board.size(); ++i) {
                    LinkedList<Character> tmp = (LinkedList<Character>) node.board.clone();
                    tmp.add(i, Node.toChar(c));

                    Node n = new Node(tmp, node.hand.clone(), node.depth + 1, node);
                    --n.hand[c];
                    generated.add(n);
                }
            }
        }
        return generated;
    }


}

class Node {
    LinkedList<Character> board;
    int[] hand = new int[5];            // R,G,B,Y,W
    int depth;
    LinkedList<Node> history = new LinkedList<>();

    public Node (LinkedList<Character> board, int[] hand, int depth, Node n) {
        this.board = board;
        this.hand = hand;
        this.depth = depth;
        history = n.history;
        history.addLast(n);
    }

    public Node (String board, String hand, int depth) {
        LinkedList<Character> tmp = new LinkedList<>();
        for (char c : board.toCharArray()) {
            tmp.add(c);
        }
        this.board = tmp;

        for (char c : hand.toCharArray()) {
            ++this.hand[Node.fromChar(c)];
        }
        this.depth = depth;
    }

    private static int fromChar(char c) {
        if (c == 'R')
            return 0;
        else if (c == 'G')
            return 1;
        else if (c == 'B')
            return 2;
        else if (c == 'Y')
            return 3;
        else if (c == 'W')
            return 4;
        return -1;
    }

    public void removeTriples() {
        int preSize;
        do {
            preSize = board.size();
            removeTriplesSub();
        } while (preSize != board.size() && board.size() > 0);
    }

    private void removeTriplesSub() {
        char preChar = board.getFirst();
        int preIdx = 0;
        for (int i = 1; i < board.size(); ++i) {
            if (preChar != board.get(i)) {
                if (i - preIdx >= 3) {
                    if (i > preIdx) {
                        board.subList(preIdx, i).clear();
                    }
                    return;
                }

                preChar = board.get(i);
                preIdx = i;
            }
        }
        if (board.size() - preIdx >= 3) {
            board.subList(preIdx, board.size()).clear();
        }
    }

    public static Character toChar(int c) {
        if (c == 0)
            return 'R';
        if (c == 1)
            return 'G';
        if (c == 2)
            return 'B';
        if (c == 3)
            return 'Y';
        if (c == 4)
            return 'W';
        return ' ';
    }

    public int boardDifferent() {
        int[] k = new int[5];
        for (char c : board) {
            k[fromChar(c)] = 1;
        }
        int sum = 0;
        for (int i : k) {
            sum += i;
        }
        return sum;
    }
}
