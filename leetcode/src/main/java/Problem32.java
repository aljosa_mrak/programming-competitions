import java.util.Set;
import java.util.TreeSet;

/**
 * Created by PP on 14.10.2018.
 */
public class Problem32 {

    public static void main(String[] args) {
        Set<Long> sum = new TreeSet<>();
        for (long i = 0; i < 2000; ++i) {
            for (long j = i; j < 2000; j++) {
                long k = i * j;

                if (is1to9Pandigital(i, j, k)) {
                    System.out.println(i);
                    System.out.println(j);
                    System.out.println(k);
                    System.out.println();
                    sum.add(k);
                }
            }
        }
        int s = 0;
        for (Long l : sum) {
            s += l;
        }

        System.out.println(s);
    }

    private static boolean is1to9Pandigital(long i, long j, long k) {
        return is1to9Pandigital(i + String.valueOf(j) + k);
    }

    private static boolean is1to9Pandigital(String s) {
        if (s.length() != 9) {
            return false;
        }

        for (long i = 1; i <= 9; i++) {
            if (!s.contains(i + ""))
                return false;
        }
        return true;
    }
}
