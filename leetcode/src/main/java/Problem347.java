import java.util.*;

/**
 * Created by PP on 14.10.2018.
 */
public class Problem347 {

    public static void main(String[] args) {
        System.out.println(topKFrequent(new int[] {2,3,4,1,4,0,4,-1,-2,-1}, 2));
    }


    public static List<Integer> topKFrequent(int[] nums, final int k) {
        HashMap<Integer, Integer> m = new HashMap<>();
        for (Integer n : nums) {
            m.merge(n, 1, Integer::sum);
        }

        Map<Integer, Integer> res = new LinkedHashMap<>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
                return this.size() > k;
            }
        };
        int min = 0;

        for (Map.Entry<Integer, Integer> e: m.entrySet()) {
            if (e.getValue() > min) {
                res.put(e.getKey(), 0);
                if (res.size() >= k) {
                    min = e.getValue();
                }
            }
        }
        List<Integer> l = new ArrayList(k);
        for (Map.Entry<Integer, Integer> e: m.entrySet()) {
            if (e.getValue() >= min) {
                l.add(e.getKey());
            }
        }
        Collections.sort(l);
        return l;
    }
}