import java.util.*;

/**
 * Created by PP on 14.10.2018.
 */
public class Problem49 {

    private static final Set<Integer> primes = Utils.getPrimes();
    private static final List<Integer> primesList = new ArrayList<>(Utils.getPrimes());

    public static void main(String[] args) {
        Collections.sort(primesList);

        // find first prime number greater than 1000
        int i = 0;
        while(primesList.get(i) < 1000) {
            ++i;
        }

        while (i < primesList.size()) {
            checkPermutations(primesList.get(i));
            ++i;
        }
    }

    private static void checkPermutations(int number) {
        HashSet<Integer> permutations = getPermutations("", String.valueOf(number));

        int i = 0;
        for (Integer p : permutations) {
            if (primes.contains(p)) {
                ++i;
                if (i >= 3) {
                    List<Integer> tmp = new ArrayList<>(permutations);
                    Collections.sort(tmp);

                    List<Integer> tmp1 = new ArrayList<>();
                    for (int idx = 1; idx < tmp.size(); ++idx) {
                        tmp1.add(tmp.get(idx) - tmp.get(idx-1));
                    }

                    for (int idx = 1; idx < tmp1.size(); ++idx) {
                        if (tmp1.get(idx).equals(tmp1.get(idx - 1))) {
                                System.out.println(tmp1);
                                System.out.println(tmp);
                                System.out.println();
                                return;
                        }
                    }
                    return;
                }
            }
        }

    }

    private static HashSet<Integer> getPermutations(String prefix, String str) {
        HashSet<Integer> permutations = new HashSet<>();
        int n = str.length();
        if (n == 0) {
            permutations.add(Integer.parseInt(prefix));
        } else {
            for (int i = 0; i < n; i++) {
                permutations.addAll(getPermutations(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n)));
            }
        }
        return permutations;
    }

}
