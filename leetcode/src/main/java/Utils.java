import java.util.HashSet;
import java.util.Set;

/**
 * Created by PP on 14.10.2018.
 */
public class Utils {

    static Set<Integer> getPrimes() {
        // initially assume all integers are prime
        boolean[] isPrime = new boolean[10000 + 1];
        for (int i = 2; i <= 10000; i++) {
            isPrime[i] = true;
        }

        // mark non-primes <= n using Sieve of Eratosthenes
        for (int factor = 2; factor * factor <= 10000; factor++) {

            // if factor is prime, then mark multiples of factor as non prime
            // suffices to consider multiples factor, factor+1, ...,  n/factor
            if (isPrime[factor]) {
                for (int j = factor; factor * j <= 10000; j++) {
                    isPrime[factor * j] = false;
                }
            }
        }

        Set<Integer> primes = new HashSet<>((int) (10000 / Math.log(10000)));

        for (int i = 2; i <= 10000; i++) {
            if (isPrime[i]) {
                primes.add(i);
            }
        }
        return primes;
    }
}
