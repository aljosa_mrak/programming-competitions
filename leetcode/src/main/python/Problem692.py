# Top K Frequent Words

from collections import Counter
from typing import List


class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        return [x[0] for x in sorted(Counter(words).items(), key=lambda x: (-x[1], x[0]))[:k]]


solution = Solution()
print(solution.topKFrequent(["love", "i", "leetcode", "i", "love", "coding"], 2))
print(solution.topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], 4))
print(solution.topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], 4))
